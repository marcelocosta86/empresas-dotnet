﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Resources;
using IOASYS.Desafio.Dominio.Validator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Dominio.Commands.Login
{
    public class LoginCommand : Notifiable, ISelfValidator, IRequest<ICommandResult>
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public bool IsValid()
        {
            try
            {
                if (string.IsNullOrEmpty(Login))
                    AddNotification("Login", Message.Campo_obrigatorio);
                else if (Login.Length > 50)
                    AddNotification("Login", Message.Tamanho_excedido);

                if (string.IsNullOrEmpty(Password))
                    AddNotification("Senha", Message.Campo_obrigatorio);
                else if (!(Password.Length >= 3 && Password.Length <= 6))
                    AddNotification("Senha", Message.Tamanho_excedido);

                return Valid;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
