﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Resources;
using IOASYS.Desafio.Dominio.Validator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Dominio.Commands.Movie
{
    public class RegisterNewMovieCommand : Notifiable, ISelfValidator, IRequest<ICommandResult>
    {
        public int MovieId { get; set; }
        public string NameMovie { get; set; }
        public string Director { get; set; }
        public string Gender { get; set; }
        public ICollection<Actor> Actors { get; set; }
        public bool IsValid()
        {

            try
            {
                if (string.IsNullOrEmpty(NameMovie))
                    AddNotification("NameMovie", Message.Campo_obrigatorio);
                else if (NameMovie.Length > 50)
                    AddNotification("NameMovie", Message.Tamanho_excedido);

                if (string.IsNullOrEmpty(Director))
                    AddNotification("Director", Message.Campo_obrigatorio);
                else if (Director.Length > 50)
                    AddNotification("Director", Message.Tamanho_excedido);

                if (string.IsNullOrEmpty(Gender))
                    AddNotification("Gender", Message.Campo_obrigatorio);
                else if (Gender.Length > 50)
                    AddNotification("Gender", Message.Tamanho_excedido);

                if (Actors.Count <= 0)
                    AddNotification("Actors", Message.Campo_obrigatorio);

                return Valid;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
