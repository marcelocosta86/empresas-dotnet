﻿namespace IOASYS.Desafio.Dominio.Entidades
{
    public class Rating
    {
        public int RatingId { get; private set; }
        public Account Account { get; private set; }
        public int AccountId { get; private set; }
        public Movie Movie { get; private set; }
        public int MovieId { get; private set; }
        public int Points { get; private set; }

        public Rating(int accountId, int movieId, int points)
        {
            AccountId = accountId;
            MovieId = movieId;
            Points = points;
        }
    }
}
