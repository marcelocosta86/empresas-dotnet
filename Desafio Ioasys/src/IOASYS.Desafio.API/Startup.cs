﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.UserSecrets;
using Microsoft.Extensions.DependencyInjection;
using IOASYS.Desafio.API.Configuration;
using Microsoft.Extensions.Hosting;
using MediatR;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.EntityFrameworkCore;
using IOASYS.Desafio.Infra.DataContexts;
using IOASYS.Desafio.Dominio.Autenticacao;
using IOASYS.Desafio.Dominio.Configuration;
using IOASYS.Desafio.Infra.Settings;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Infra.Repositories;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using IOASYS.Desafio.Infra.Repositories.ReadOnly;
using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.Service;
using IOASYS.Desafio.Dominio.Commands.Account;
using System.Reflection;
using IOASYS.Desafio.Dominio.Commands.Movie;
using IOASYS.Desafio.Dominio.Commands.Rating;

namespace IOASYS.Desafio.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<VotacaoDbContext>(item => item.UseSqlServer(Configuration.GetSection("SettingsInfra:ConnectionString").Value));

            services.Configure<Settings>(options => Configuration.GetSection("Settings").Bind(options));
            services.Configure<SettingsInfra>(options => Configuration.GetSection("SettingsInfra").Bind(options));

            services.AddControllers();

            services.AddMemoryCache();

            // AutoMapper Settings
            services.AddAutoMapperConfiguration();

            // Handlers
            services.AddMediatR(typeof(Startup));
            services.AddMediatR(typeof(RegisterNewAccountCommand).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(UpdateNewAccountCommand).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(DeleteNewAccountCommand).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(RegisterNewMovieCommand).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(RegisterRatingCommand).GetTypeInfo().Assembly);


            // Application
            services.AddScoped<IAccountAppService, AccountAppService>();
            services.AddScoped<IMovieAppService, MovieAppService>();
            services.AddScoped<IRatingAppService, RatingAppService>();
            services.AddTransient<TokenService>();

            // Infra - Data
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IUserRepositoryReadOnly, UserRepositoryReadOnly>();
            services.AddScoped<IMovieRepositoryReadOnly, MovieRepositoryReadOnly>();
            services.AddScoped<IRatingRepositoryReadOnly, RatingRepositoryReadOnly>();

            #region [+] AutenticacaoJWT
            var key = Encoding.ASCII.GetBytes(Configuration.GetValue<string>("Settings:SecretKey"));
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddTransient<TokenService>();
            #endregion

            services.AddSwaggerDocumentation();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(c =>
            {
                c.AllowAnyHeader();
                c.AllowAnyMethod();
                c.AllowAnyOrigin();
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwaggerDocumentation();
        }
    }
}
