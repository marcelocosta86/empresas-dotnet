﻿using AutoMapper;
using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.Interfaces.CommandResult;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Application.ViewModel.DTO;
using IOASYS.Desafio.Dominio.Commands.Account;
using IOASYS.Desafio.Dominio.Commands.Login;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Application.Service
{
    public class AccountAppService : IAccountAppService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRepositoryReadOnly _userRepositoryReadOnly;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public AccountAppService(IMapper mapper, IMediator mediator,
               IUserRepository userRepository, IUserRepositoryReadOnly userRepositoryReadOnly)
        {
            _userRepository = userRepository;
            _userRepositoryReadOnly = userRepositoryReadOnly;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<UserViewModel> GetByIdAsync(int id)
        {
            return _mapper.Map<UserViewModel>(await _userRepositoryReadOnly.ObterPorIdAsync(id));
        }

        public async Task<IEnumerable<UserViewModel>> ListAsync()
        {
            return _mapper.Map<IEnumerable<UserViewModel>>(await _userRepositoryReadOnly.ListarAsync());
        }

        public async Task<CommandResultViewModel> Delete(int id)
        {
            var removeCommand = new DeleteNewAccountCommand() { AccountId = id };
            return _mapper.Map<CommandResultViewModel>(await _mediator.Send(removeCommand));
        }      

        public async Task<CommandResultViewModel> Insert(UserViewModel user)
        {
            var registerCommand = _mapper.Map<RegisterNewAccountCommand>(user);
            return _mapper.Map<CommandResultViewModel>(await _mediator.Send(registerCommand));
        }

        public async Task<CommandResultViewModel> ObterToken(LoginViewModel login)
        {
            var registerCommand = _mapper.Map<LoginCommand>(login);
            return _mapper.Map<CommandResultViewModel>(await _mediator.Send(registerCommand));
        }

        public async Task<CommandResultViewModel> Update(UserViewModel user)
        {
            var updateCommand = _mapper.Map<UpdateNewAccountCommand>(user);
            return _mapper.Map<CommandResultViewModel>(await _mediator.Send(updateCommand));
        }
    }
}
