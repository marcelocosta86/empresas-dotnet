﻿using System;
using MediatR;

namespace IOASYS.Desafio.Application
{
    public interface ICommand : IRequest
    {
        int Id { get; }
    }

    public interface ICommand<out TResult> : IRequest<TResult>
    {
        int Id { get; }
    }
}