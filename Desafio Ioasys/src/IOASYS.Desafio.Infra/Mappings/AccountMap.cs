﻿using IOASYS.Desafio.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IOASYS.Desafio.Infra.Mappings
{
    public class AccountMap : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasKey(a => a.AccountId);

            builder.Property(a => a.AccountId).ValueGeneratedOnAdd().UseIdentityColumn();

            builder.Property(c => c.Login)
                .HasColumnType("varchar(50)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.UserName)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
