﻿using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Infra.DataContexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Infra.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        protected readonly VotacaoDbContext Db;
        protected readonly DbSet<Movie> DbSet;

        public MovieRepository(VotacaoDbContext context)
        {
            Db = context;
            DbSet = Db.Set<Movie>();
        }

        public async Task Add(Movie movie)
        {
            await DbSet.AddAsync(movie);
            await Db.SaveChangesAsync();
        }

        public void Remove(Movie movie)
        {
            DbSet.Remove(movie);
            Db.SaveChanges();
        }

        public void Update(Movie movie)
        {
            DbSet.Update(movie);
            Db.SaveChanges();
        }
    }
}
