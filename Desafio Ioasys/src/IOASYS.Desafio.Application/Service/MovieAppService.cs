﻿using AutoMapper;
using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Dominio.Commands.Movie;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Application.Service
{
    public class MovieAppService : IMovieAppService
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        private readonly IMovieRepository _movieRepository;
        private readonly IMovieRepositoryReadOnly _movieRepositoryReadOnly;

        public MovieAppService(IMapper mapper, IMediator mediator, IMovieRepository movieRepository, IMovieRepositoryReadOnly movieRepositoryReadOnly)
        {
            _mapper = mapper;
            _mediator = mediator;
            _movieRepository = movieRepository;
            _movieRepositoryReadOnly = movieRepositoryReadOnly;

        }

        public async Task<CommandResultViewModel> Insert(MovieViewModel movie)
        {
            var registerCommand = _mapper.Map<RegisterNewMovieCommand>(movie);
            return _mapper.Map<CommandResultViewModel>(await _mediator.Send(registerCommand));
        }

        public async Task<MovieViewModel> GetByIdAsync(int id)
        {
            return _mapper.Map<MovieViewModel>(await _movieRepositoryReadOnly.ObterPorIdAsync(id));
        }

        public async Task<IEnumerable<MovieViewModel>> ListRankAsync()
        {
            return _mapper.Map<IEnumerable<MovieViewModel>>(await _movieRepositoryReadOnly.ListRankAsync());
        }

        public async Task<IEnumerable<MovieViewModel>> ListByFilterAsync(string name, string director, string gender, string actor)
        {
            return _mapper.Map<IEnumerable<MovieViewModel>>(await _movieRepositoryReadOnly.ListByFilterAsync(name, director, gender, actor));
        }
    }
}
