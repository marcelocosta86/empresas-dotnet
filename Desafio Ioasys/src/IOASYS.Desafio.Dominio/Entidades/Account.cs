﻿namespace IOASYS.Desafio.Dominio.Entidades
{
    public class Account 
    {
        public int AccountId { get; private set; }
        public string UserName { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string Role { get; private set; }
        public bool Active { get; private set; }

        public Account(string userName, string login, string password, string role, bool active = true)
        {
            UserName = userName;
            Login = login;
            Password = password;
            Role = role;
            Active = active;
        }

        public Account(int accountId, string userName, string login, string password, string role, bool active = true)
        {
            AccountId = accountId;
            UserName = userName;
            Login = login;
            Password = password;
            Role = role;
            Active = active;
        }

        public void SetInactive()
        {
            Active = false;
        }
    }
}
