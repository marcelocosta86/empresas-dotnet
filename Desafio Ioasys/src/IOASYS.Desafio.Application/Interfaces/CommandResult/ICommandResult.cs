﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.Interfaces.CommandResult
{
    public interface ICommandResult
    {
        bool Success { get; set; }
        string Message { get; set; }
        object Data { get; set; }
    }
}
