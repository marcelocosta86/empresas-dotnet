﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.Validator
{
    interface ISelfValidator
    {
        bool IsValid();
    }
}
