﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Commands;
using IOASYS.Desafio.Dominio.Commands.Movie;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Extension;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Dominio.Resources;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Handlers
{
    public class MovieHandler : Notifiable,
        IRequestHandler<RegisterNewMovieCommand, ICommandResult>
        
    {
        private readonly IMovieRepository _movieRepository;

        public MovieHandler(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task<ICommandResult> Handle(RegisterNewMovieCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                    return new CommandResult(false, Message.Inconsistencias_dados, command.Notifications);

                if (Invalid)
                    return new CommandResult(false, Message.Inconsistencias_dados, Notifications);

                Movie movie = new Movie(command.NameMovie, command.Director, command.Gender);
                movie.Actors = command.Actors;
                await _movieRepository.Add(movie);

                return new CommandResult(true, Message.Operacao_sucesso,
                    new { movie.MovieId, movie.NameMovie, movie.Director, movie.Gender});
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
