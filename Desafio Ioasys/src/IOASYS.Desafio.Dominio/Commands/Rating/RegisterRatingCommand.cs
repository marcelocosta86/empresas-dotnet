﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Resources;
using IOASYS.Desafio.Dominio.Validator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Dominio.Commands.Rating
{
    public class RegisterRatingCommand : Notifiable, ISelfValidator, IRequest<ICommandResult>
    {
        public int AccountId { get; private set; }
        public int MovieId { get; private set; }
        public int Points { get; private set; }
        public bool IsValid()
        {

            try
            {
                if (AccountId == default)
                    AddNotification("AccountId", Message.Campo_obrigatorio);

                if (MovieId == default)
                    AddNotification("MovieId", Message.Campo_obrigatorio);

                if (Points > 4)
                    AddNotification("Points", Message.Pontuacao_Invalida);

                return Valid;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
