﻿using Flunt.Notifications;
using IOASYS.Desafio.Application;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Resources;
using IOASYS.Desafio.Dominio.Validator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Dominio.Commands.Account
{
    public class RegisterNewAccountCommand : Notifiable, ISelfValidator, IRequest<ICommandResult>
    {
        public int AccountId { get; set; }
        public string UserName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool Active { get; set; }

        public bool IsValid()
        {
            try
            {
                if (string.IsNullOrEmpty(UserName))
                    AddNotification("UserName", Message.Campo_obrigatorio);
                else if (UserName.Length > 50)
                    AddNotification("UserName", Message.Tamanho_excedido);

                if (string.IsNullOrEmpty(Login))
                    AddNotification("Login", Message.Campo_obrigatorio);
                else if (Login.Length > 50)
                    AddNotification("Login", Message.Tamanho_excedido);

                if (string.IsNullOrEmpty(Password))
                    AddNotification("Password", Message.Campo_obrigatorio);
                else if (!(Password.Length >= 3 && Password.Length <= 6))
                    AddNotification("Password", Message.Tamanho_excedido);

                if (string.IsNullOrEmpty(Role))
                    AddNotification("Role", Message.Campo_obrigatorio);

                return Valid;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
