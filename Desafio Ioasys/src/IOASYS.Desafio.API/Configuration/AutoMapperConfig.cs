﻿using IOASYS.Desafio.Application.AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;


namespace IOASYS.Desafio.API.Configuration
{
    public static class AutoMapperConfig
    {
        public static void AddAutoMapperConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(DomainToViewModelMappingProfile), typeof(ViewModelToDomainMappingProfile));
        }
    }
}
