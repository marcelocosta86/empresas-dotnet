﻿using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Application.ViewModel.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IOASYS.Desafio.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountAppService _accountAppService;

        public AccountController(IAccountAppService accountAppService)
        {
            _accountAppService = accountAppService;
        }

        [HttpPost]
        [Route("v1/token")]
        [AllowAnonymous]
        public async Task<CommandResultViewModel> Token([FromBody] LoginViewModel login)
        {
            return await _accountAppService.ObterToken(login);
        }

        /// <summary>
        /// Usuarios
        /// </summary>                
        /// <remarks><h2><b>Lista todos os Usuarios não administradores ativos.</b></h2></remarks>
        /// <response code="200">OK Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [Route("v1/users")]
        [Authorize(Roles = "Visitor,Admin")]
        public async Task<IEnumerable<UserViewModel>> GetUsers()
        {
            return await _accountAppService.ListAsync();
        }

        /// <summary>
        /// Usuarios
        /// </summary>                
        /// <remarks><h2><b>Consulta o Usuario.</b></h2></remarks>
        /// <param name="id">Parâmetro requerido id do Usuario</param>
        /// <response code="200">OK Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [Route("v1/user/{id}")]
        [Authorize(Roles = "Visitor,Admin")]
        public async Task<UserViewModel> GeyById(int id)
        {
            return await _accountAppService.GetByIdAsync(id);
        }

        /// <summary>
        /// Incluir Usuario 
        /// </summary>                
        /// <remarks><h2><b>Incluir novo Usuario. Roles: Visitor, Admin</b></h2></remarks>
        /// <param name="user">Parâmetro requerido command de Insert</param>
        /// <response code="200">OK Request</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [Route("v1/user")]
        [AllowAnonymous]
        public async Task<CommandResultViewModel> InsertUser([FromBody] UserViewModel user)
        {
            return await _accountAppService.Insert(user);
        }

        /// <summary>
        /// Alterar Usuario
        /// </summary>        
        /// <remarks><h2><b>Alterar Usuario. Roles: Visitor, Admin</b></h2></remarks>
        /// <param name="id">Parâmetro requerido id do Usuario</param>        
        /// <param name="user">Parâmetro requerido command de Update</param>
        /// <response code="200">OK Request</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut]
        [Route("v1/user")]
        [Authorize(Roles = "Visitor,Admin")]
        public async Task<CommandResultViewModel> AlterUser([FromBody] UserViewModel user)
        {
            return await _accountAppService.Update(user);
        }

        /// <summary>
        /// Excluir Usuario
        /// </summary>                
        /// <remarks><h2><b>Excluir Usuario.</b></h2></remarks>
        /// <param name="id">Parâmetro requerido id do Usuario</param>        
        /// <response code="200">OK Request</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Internal Server Error</response>
        [HttpDelete]
        [Route("v1/user/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<CommandResultViewModel> DeleteUser(int id)
        {
            return await _accountAppService.Delete(id);
        }
    }
}
