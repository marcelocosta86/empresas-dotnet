﻿using Dapper;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using IOASYS.Desafio.Infra.Settings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Slapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Infra.Repositories.ReadOnly
{
    public class MovieRepositoryReadOnly : IMovieRepositoryReadOnly
    {
        private readonly DynamicParameters _parametros = new DynamicParameters();
        private readonly IDbConnection conexao;

        public MovieRepositoryReadOnly(IOptions<SettingsInfra> options)
        {
            conexao = new SqlConnection(options.Value.ConnectionString);
        }

       
        public async Task<IEnumerable<Movie>> ListRankAsync()
        {
            try
            {
                var sql = @"select 
                              RANK() OVER (ORDER BY AVG(Points) desc) AS Rank, 
                              m.MovieId,
                              m.NameMovie,
                              m.Director,
                              m.Gender,
                              AVG(Points) RatingAvg
                            from Movie m
                              left join Actor a
                              on m.MovieId = a.MovieId
                              left join Rating r
                              on r.MovieId = m.MovieId
                            Group by 
                              m.MovieId,
                              m.NameMovie,
                              m.Director,
                              m.Gender;";
                            
                var result = await conexao.QueryAsync<Movie>(sql);

                return result.ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Movie> ObterPorIdAsync(int id)
        {
            try
            {
                _parametros.Add("Id", id, DbType.Int32);

                var sql = @"select   
                              m.MovieId,
                              m.NameMovie,
                              m.Director,
                              m.Gender,
                              AVG(Points) as RatingAvg,
                              a.ActorId as Actors_ActorId,
                              a.NameAtor as Actors_NameAtor
                            from Movie m
                              left join Actor a
                              on m.MovieId = a.MovieId
                              left join Rating r
                              on r.MovieId = m.MovieId
                            
                            Where m.MovieId = @Id
                            Group by 
                              m.MovieId,
                              m.NameMovie,
                              m.Director,
                              m.Gender,
                              a.ActorId,
                              a.NameAtor;";


                var resultado = await conexao.QueryAsync<dynamic>(sql, _parametros);

                AutoMapper.Configuration.AddIdentifier(
                        typeof(Movie), "MovieId");

                var movie = AutoMapper.MapDynamic<Movie>(resultado).FirstOrDefault();

                return movie;
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<bool> CheckIdAsync(int id)
        {
            try
            {
                _parametros.Add("Id", id, DbType.Int32);

                var sql = @"SELECT * FROM Movie WHERE MovieId=@Id;";

                var result = await conexao.QueryAsync<Movie>(sql, _parametros);

                if (result.Count() > 0)
                    return true;

                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<IEnumerable<Movie>> ListByFilterAsync(string nome, string diretor, string genero, string atores)
        {
            try
            {
                _parametros.Add("Name", nome, DbType.String);
                _parametros.Add("Director", diretor, DbType.String);
                _parametros.Add("Gender", genero, DbType.String);
                _parametros.Add("Actor", atores, DbType.String);

                var sql = @"select distinct   
                              m.MovieId,
                              m.NameMovie,
                              m.Director,
                              m.Gender
                            from Movie m
                              left join Actor a
                              on m.MovieId = a.MovieId";

                var where = " Where m.MovieId is not null ";

                where += !string.IsNullOrEmpty(nome) ? " and  m.NameMovie = @Name " : "";
                where += !string.IsNullOrEmpty(diretor) ? " and  m.Director = @Director " : "";
                where += !string.IsNullOrEmpty(genero) ? " and m.Gender = @Gender " : "";
                where += !string.IsNullOrEmpty(atores) ? " and a.NameAtor = @Actor " : "";

                sql += where;

                sql += " Order by NameMovie ";

                var result = await conexao.QueryAsync<Movie>(sql, _parametros);
                return result.ToList();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
