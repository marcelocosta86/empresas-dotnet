﻿using AutoMapper;
using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.Interfaces.CommandResult;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Application.ViewModel.DTO;
using IOASYS.Desafio.Dominio.Commands.Account;
using IOASYS.Desafio.Dominio.Commands.Login;
using IOASYS.Desafio.Dominio.Commands.Rating;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Application.Service
{
    public class RatingAppService : IRatingAppService
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public RatingAppService(IMapper mapper, IMediator mediator)
        {
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<CommandResultViewModel> VoteAsync(RatingViewModel command)
        {
            var registerCommand = _mapper.Map<RegisterRatingCommand>(command);
            return _mapper.Map<CommandResultViewModel>(await _mediator.Send(registerCommand));
        }
    }
}
