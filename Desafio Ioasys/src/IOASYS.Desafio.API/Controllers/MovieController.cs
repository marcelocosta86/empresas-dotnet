﻿using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOASYS.Desafio.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ApiController]
    public class MovieController : ControllerBase
    {

        private readonly IMovieAppService _movieAppService;

        public MovieController(IMovieAppService movieAppService)
        {
            _movieAppService = movieAppService;
        }

        /// <summary>
        /// Filmes
        /// </summary>                
        /// <remarks><h2><b>Lista os Filmes por osrdem de mais votado.</b></h2></remarks>
        /// <response code="200">OK Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [Route("v1/movies-ranking")]
        [AllowAnonymous]
        public async Task<IEnumerable<MovieViewModel>> ListRank()
        {
            return await _movieAppService.ListRankAsync();
        }

        /// <summary>
        /// Filmes
        /// </summary>                
        /// <remarks><h2><b>Lista os Filmes por filtros.</b></h2></remarks>
        /// <response code="200">OK Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [Route("v1/movies-filter")]
        [AllowAnonymous]
        public async Task<IEnumerable<MovieViewModel>> ListByFilter(string nome, string diretor, string genero, string atores)
        {
            return await _movieAppService.ListByFilterAsync(nome, diretor, genero, atores);
        }

        /// <summary>
        /// Filmes
        /// </summary>                
        /// <remarks><h2><b>Consulta o Filme com media de votos.</b></h2></remarks>
        /// <param name="id">Parâmetro requerido id do Filme</param>
        /// <response code="200">OK Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [Route("v1/movie/{id}")]
        [AllowAnonymous]
        public async Task<MovieViewModel> GeyById(int id)
        {
            return await _movieAppService.GetByIdAsync(id);
        }

        /// <summary>
        /// Incluir Filme 
        /// </summary>                
        /// <remarks><h2><b>Administrador pode Incluir novo Filme.</b></h2></remarks>
        /// <param name="command">Parâmetro requerido command de Insert</param>
        /// <response code="200">OK Request</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [Route("v1/movie")]
        [Authorize(Roles = "Admin")]
        public async Task<CommandResultViewModel> FilmeInserirAsync([FromBody] MovieViewModel movie)
        {
            return await _movieAppService.Insert(movie);
        }
    }
}
