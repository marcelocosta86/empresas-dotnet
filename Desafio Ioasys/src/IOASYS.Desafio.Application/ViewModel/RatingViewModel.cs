﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.ViewModel
{
    public class RatingViewModel
    {
        public int RatingId { get; set; }
        public int AccountId { get; set; }
        public int MovieId { get; set; }
        public int Points { get; set; }
    }
}
