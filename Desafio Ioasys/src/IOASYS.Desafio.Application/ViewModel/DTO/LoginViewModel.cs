﻿namespace IOASYS.Desafio.Application.ViewModel.DTO
{
    public class LoginViewModel
    { 
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
