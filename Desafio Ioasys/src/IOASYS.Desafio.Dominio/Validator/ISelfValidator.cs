﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Dominio.Validator
{
    public interface ISelfValidator
    {
        bool IsValid();
    }
}
