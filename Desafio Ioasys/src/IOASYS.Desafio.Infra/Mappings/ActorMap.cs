﻿using IOASYS.Desafio.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IOASYS.Desafio.Infra.Mappings
{
    public class ActorMap : IEntityTypeConfiguration<Actor>
    {
        public void Configure(EntityTypeBuilder<Actor> builder)
        {
            builder.HasKey(a => a.ActorId);

            builder.Property(a => a.ActorId).ValueGeneratedOnAdd().UseIdentityColumn();

            builder.Property(c => c.NameAtor)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

        }
    }
}
