﻿using IOASYS.Desafio.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Interfaces.Repository
{
    public interface IMovieRepository
    {
        Task Add(Movie movie);
        void Update(Movie movie);
        void Remove(Movie movie);
    }
}
