﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Resources;
using IOASYS.Desafio.Dominio.Validator;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Dominio.Commands.Account
{
    public class DeleteNewAccountCommand : Notifiable, ISelfValidator, IRequest<ICommandResult>
    {
        public int AccountId { get; set; }

        public bool IsValid()
        {
            try
            {
                if (AccountId == default)
                    AddNotification("AccountId", Message.Campo_obrigatorio);

                return Valid;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
