﻿using AutoMapper;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Application.ViewModel.DTO;
using IOASYS.Desafio.Dominio.Commands.Account;
using IOASYS.Desafio.Dominio.Commands.Login;
using IOASYS.Desafio.Dominio.Commands.Movie;
using IOASYS.Desafio.Dominio.Commands.Rating;
using IOASYS.Desafio.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<UserViewModel, RegisterNewAccountCommand>();
            CreateMap<UserViewModel, UpdateNewAccountCommand>();
            CreateMap<UserViewModel, DeleteNewAccountCommand>();
            CreateMap<MovieViewModel, RegisterNewMovieCommand>();
            CreateMap<MovieViewModel, Movie>();
            CreateMap<ActorViewModel, Actor>();
            CreateMap<RatingViewModel, RegisterRatingCommand>();
            CreateMap<LoginViewModel, LoginCommand>();
        }
    }
}
