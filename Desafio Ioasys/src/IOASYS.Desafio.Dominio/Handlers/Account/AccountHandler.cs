﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Autenticacao;
using IOASYS.Desafio.Dominio.Commands;
using IOASYS.Desafio.Dominio.Commands.Account;
using IOASYS.Desafio.Dominio.Commands.Login;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Extension;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using IOASYS.Desafio.Dominio.Resources;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Handlers
{
    public class AccountHandler : Notifiable,
        IRequestHandler<RegisterNewAccountCommand, ICommandResult>,
        IRequestHandler<UpdateNewAccountCommand, ICommandResult>,
        IRequestHandler<DeleteNewAccountCommand, ICommandResult>,
        IRequestHandler<LoginCommand, ICommandResult>
        
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRepositoryReadOnly _userRepositoryReadOnly;
        private readonly TokenService _tokenService;

        public AccountHandler(IUserRepository userRepository, IUserRepositoryReadOnly userRepositoryReadOnly,
                              TokenService tokenService)
        {
            _userRepository = userRepository;
            _userRepositoryReadOnly = userRepositoryReadOnly;
            _tokenService = tokenService;

        }

        public async Task<ICommandResult> Handle(RegisterNewAccountCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                    return new CommandResult(false, Message.Inconsistencias_dados, command.Notifications);

                if (await _userRepositoryReadOnly.CheckLoginAsync(command.Login))
                    AddNotification("Login", Message.Login_existente);

                if (Invalid)
                    return new CommandResult(false, Message.Inconsistencias_dados, Notifications);

                Account user = new Account(command.UserName, command.Login, Criptografia.Criptografa(command.Password), command.Role);

                await _userRepository.Add(user);

                return new CommandResult(true, Message.Operacao_sucesso,
                    new { user.AccountId, user.UserName, user.Login, Role = user.Role, Senha = "******" });
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<ICommandResult> Handle(UpdateNewAccountCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                    return new CommandResult(false, Message.Inconsistencias_dados, command.Notifications);

                if (!await _userRepositoryReadOnly.CheckIdAsync(command.AccountId))
                    AddNotification("Id", Message.Usuario_nao_cadastrado);

                if (Invalid)
                    return new CommandResult(false, Message.Inconsistencias_dados, Notifications);

                Account user = new Account(command.AccountId, command.UserName, command.Login, Criptografia.Criptografa(command.Password), command.Role);

                _userRepository.Update(user);

                return new CommandResult(true, Message.Operacao_sucesso,
                    new { user.AccountId, user.UserName, user.Login, Role = user.Role, Senha = "******" });
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<ICommandResult> Handle(DeleteNewAccountCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                    return new CommandResult(false, Message.Inconsistencias_dados, command.Notifications);

                if (!await _userRepositoryReadOnly.CheckIdAsync(command.AccountId))
                    AddNotification("Id", Message.Usuario_nao_cadastrado);

                if (Invalid)
                    return new CommandResult(false, Message.Inconsistencias_dados, Notifications);

                var user = await _userRepositoryReadOnly.ObterPorIdAsync(command.AccountId);
                _userRepository.Remove(user);

                return new CommandResult(true, Message.Operacao_sucesso,
                    new { command.AccountId });
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<ICommandResult> Handle(LoginCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                    return new CommandResult(false, Message.Inconsistencias_dados, command.Notifications);

                if (!await _userRepositoryReadOnly.CheckAutenticacaoAsync(command.Login, Criptografia.Criptografa(command.Password)))
                    AddNotification("Autenticação", Message.Login_ou_Senha_invalidos);

                if (Invalid)
                    return new CommandResult(false, Message.Inconsistencias_dados, Notifications);

                var usuarioResult = await _userRepositoryReadOnly.ObterPorLoginAsync(command.Login);

                var token = _tokenService.GenerateToken(usuarioResult.Login, usuarioResult.Role);

                return new CommandResult(true, Message.Autenticacao_sucesso,
                    new { usuarioResult.Login, usuarioResult.Role, Token = token });
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
