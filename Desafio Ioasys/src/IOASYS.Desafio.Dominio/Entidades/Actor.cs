﻿using System;
using System.Collections.Generic;
using System.Text;
using IOASYS.Desafio.Dominio.Entidades;

namespace IOASYS.Desafio.Dominio.Entidades
{
    public class Actor
    {
        public int ActorId { get; private set; }
        public string NameAtor { get; private set; }
        public int MovieId { get; private set; }
        public Movie movie { get; private set; }
    }
}
