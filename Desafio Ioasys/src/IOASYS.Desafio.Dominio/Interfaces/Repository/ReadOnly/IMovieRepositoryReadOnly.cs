﻿using IOASYS.Desafio.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly
{
    public interface IMovieRepositoryReadOnly
    {
        Task<IEnumerable<Movie>> ListRankAsync();
        Task<Movie> ObterPorIdAsync(int id);
        Task<bool> CheckIdAsync(int id);
        Task<IEnumerable<Movie>> ListByFilterAsync(string nome, string diretor, string genero, string atores);
    }
}
