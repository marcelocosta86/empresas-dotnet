﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.ViewModel
{
    public class MovieViewModel
    {
        public string NameMovie { get; set; }
        public string Director { get; set; }
        public string Gender { get; set; }
        public float RatingAvg { get; set; }
        public ICollection<ActorViewModel> Actors { get; set; }
    }
}
