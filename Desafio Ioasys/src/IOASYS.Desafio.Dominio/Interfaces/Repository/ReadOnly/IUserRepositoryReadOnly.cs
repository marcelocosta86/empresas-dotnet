﻿using IOASYS.Desafio.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly
{
    public interface IUserRepositoryReadOnly
    {
        Task<List<Account>> ListarAsync();
        Task<Account> ObterPorIdAsync(int id);
        Task<Account> ObterPorLoginAsync(string login);
        Task<bool> CheckIdAsync(int id);
        Task<bool> CheckAutenticacaoAsync(string login, string senha);
        Task<bool> CheckLoginAsync(string login);
        Task<bool> CheckUsuarioVotouAsync(int id);
    }
}
