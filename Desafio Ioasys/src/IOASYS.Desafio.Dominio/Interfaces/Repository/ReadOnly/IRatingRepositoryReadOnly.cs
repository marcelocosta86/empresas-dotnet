﻿using IOASYS.Desafio.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly
{
    public interface IRatingRepositoryReadOnly
    {
        Task VoteAsync(Rating rating);
    }
}
