﻿using IOASYS.Desafio.Dominio.Entidades;
using System.Collections.Generic;

namespace IOASYS.Desafio.Dominio.Entidades
{
    public class Movie 
    {

        public int MovieId { get; private set; }
        public string NameMovie { get; private set; }
        public string Director { get; private set; }
        public string Gender { get; private set; }
        public float RatingAvg { get; set; }
        public ICollection<Actor> Actors { get; set; }

        public Movie(string nameMovie, string director, string gender)
        {
            NameMovie = nameMovie;
            Director = director;
            Gender = gender;
        }

        public Movie(int movieId, string nameMovie, string director, string gender)
        {
            MovieId = MovieId;
            NameMovie = nameMovie;
            Director = director;
            Gender = gender;
        }

        public Movie()
        {

        }

    }
}
