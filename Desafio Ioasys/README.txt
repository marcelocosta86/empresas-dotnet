*********************************************INSTRUÇÕES PARA EXECUTAR*********************************************


1-Compile o projeto e instale todas as dependencias
2-instalar o SQL Server na sua máquina

2.1) Download docker
https://download.docker.com/win/stabl...

2.2) Download SQL Server Management Studio
https://docs.microsoft.com/pt-br/sql/...
https://aka.ms/ssmsfullsetup

2.3) Garantir que esteja executando containers Linux

2.4) Baixar a última imagem estável do SQL Server para docker
docker pull mcr.microsoft.com/mssql/server:2019-latest

2.5) Criar uma nova instância utilizando essa imagem
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=@msc1213#' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2019-latest


3-Rodar o migration que foi gerado no projeto "IOASYS.Desafio.Infra" com o comando "update-database -verbose"

4-Utililizar a collection de testes que esta na pasta "Collection Postman" da raiz do repositorio


obs:Caso preferir pode restaurar o banco de dados com o backup que esta na pasta "Backup DataBase" da raiz do repositorio


