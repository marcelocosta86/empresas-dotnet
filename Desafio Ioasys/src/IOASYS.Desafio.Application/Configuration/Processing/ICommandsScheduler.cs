﻿using System.Threading.Tasks;
using MediatR;

namespace IOASYS.Desafio.Application.Configuration.Processing
{
    public interface ICommandsScheduler
    {
        Task EnqueueAsync(IRequest command);
    }
}