﻿namespace IOASYS.Desafio.Application.Configuration.DomainEvents
{
    public interface IDomainEventNotification<out TEventType>
    {
        TEventType DomainEvent { get; }
    }
}