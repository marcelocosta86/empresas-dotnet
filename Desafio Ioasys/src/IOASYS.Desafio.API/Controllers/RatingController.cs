﻿using IOASYS.Desafio.Application.Interfaces;
using IOASYS.Desafio.Application.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOASYS.Desafio.API.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly IRatingAppService _ratingAppService;

        public RatingController(IRatingAppService ratingAppService)
        {
            _ratingAppService = ratingAppService;
        }


        /// <summary>
        /// Votar
        /// </summary>                
        /// <remarks><h2><b>Usuário pode Votar no filme escolhido.</b></h2></remarks>
        /// <param name="command">Parâmetro requerido command de Insert</param>
        /// <response code="200">OK Request</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [Route("v1/vote")]
        [Authorize(Roles = "Visitor")]
        public async Task<CommandResultViewModel> VoteAsync([FromBody] RatingViewModel command)
        {
            return await _ratingAppService.VoteAsync(command);
        }
    }
}
