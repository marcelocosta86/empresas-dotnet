﻿using AutoMapper;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Application.ViewModel.DTO;
using IOASYS.Desafio.Dominio.Commands;
using IOASYS.Desafio.Dominio.Commands.Login;
using IOASYS.Desafio.Dominio.Entidades;

namespace IOASYS.Desafio.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Account, UserViewModel>();
            CreateMap<CommandResult, CommandResultViewModel>();
            CreateMap<LoginCommand, LoginViewModel>();
            CreateMap<Movie, MovieViewModel>();
            CreateMap<Rating, RatingViewModel>();
            CreateMap<Actor, ActorViewModel>();
        }
    }
}
