﻿using IOASYS.Desafio.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Application.Interfaces
{
    public interface IMovieAppService
    {
        Task<CommandResultViewModel> Insert(MovieViewModel movie);
        Task<MovieViewModel> GetByIdAsync(int id);
        Task<IEnumerable<MovieViewModel>> ListRankAsync();
        Task<IEnumerable<MovieViewModel>> ListByFilterAsync(string name, string director, string gender, string actor);
    }
}
