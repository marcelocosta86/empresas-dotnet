﻿using Dapper;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using IOASYS.Desafio.Infra.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Infra.Repositories.ReadOnly
{
    public class UserRepositoryReadOnly : IUserRepositoryReadOnly
    {
        private readonly DynamicParameters _parametros = new DynamicParameters();
        private readonly IDbConnection conexao;

        public UserRepositoryReadOnly(IOptions<SettingsInfra> options)
        {
            conexao = new SqlConnection(options.Value.ConnectionString);
        }

        public async Task<List<Account>> ListarAsync()
        {
            try
            {
                var sql = @"SELECT * FROM dbo.Account
                            WHERE Role = 'Visitor'
                            AND Active = 1
                            ORDER BY UserName;";

                var result = await conexao.QueryAsync<Account>(sql);

                return result.ToList();
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Account> ObterPorIdAsync(int id)
        {
            try
            {
                _parametros.Add("Id", id, DbType.Int32);

                var sql = @"SELECT * FROM dbo.Account WHERE AccountId=@Id AND Active = 1;";

                var result = await conexao.QueryAsync<Account>(sql, _parametros);

                return result.FirstOrDefault();
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<Account> ObterPorLoginAsync(string login)
        {
            try
            {
                _parametros.Add("Login", login, DbType.String);

                var sql = @"SELECT * FROM dbo.Account WHERE Login=@Login AND Active = 1;";

                var result = await conexao.QueryAsync<Account>(sql, _parametros);

                return result.FirstOrDefault();
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<bool> CheckIdAsync(int id)
        {
            try
            {
                _parametros.Add("Id", id, DbType.Int32);

                var sql = @"SELECT * FROM dbo.Account WHERE AccountId=@Id AND Active = 1;";

                var result = await conexao.QueryAsync<Account>(sql, _parametros);

                if (result.Count() > 0)
                    return true;

                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<bool> CheckAutenticacaoAsync(string login, string password)
        {
            try
            {
                _parametros.Add("Login", login, DbType.String);
                _parametros.Add("Password", password, DbType.String);

                var sql = @"SELECT * FROM dbo.Account 
                            WHERE Login=@Login AND Password=@Password AND Active = 1;";

                var result = await conexao.QueryAsync<Account>(sql, _parametros);

                if (result.Count() > 0)
                    return true;

                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<bool> CheckLoginAsync(string login)
        {
            try
            {
                _parametros.Add("Login", login, DbType.String);

                var sql = @"SELECT * FROM dbo.Account WHERE Login=@Login AND Active = 1;";

                var result = await conexao.QueryAsync<Account>(sql, _parametros);

                if (result.Count() > 0)
                    return true;

                return false;
            }
            catch (Exception ex) { throw ex; }
        }

        public async Task<bool> CheckUsuarioVotouAsync(int id)
        {
            try
            {
                _parametros.Add("AccountId", id, DbType.Int32);

                var sql = @"SELECT u.* FROM dbo.Account u
                            INNER JOIN dbo.Rating r ON r.AccountId = u.AccountId
                            WHERE r.AccountId=@AccountId AND u.Active = 1;";

                var result = await conexao.QueryAsync<Account>(sql, _parametros);

                if (result.Count() > 0)
                    return true;

                return false;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
