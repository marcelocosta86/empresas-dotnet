﻿using Flunt.Notifications;
using IOASYS.Desafio.Dominio.Commands;
using IOASYS.Desafio.Dominio.Commands.Movie;
using IOASYS.Desafio.Dominio.Commands.Rating;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Extension;
using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using IOASYS.Desafio.Dominio.Resources;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Handlers
{
    public class RatingHandler : Notifiable,
        IRequestHandler<RegisterRatingCommand, ICommandResult>
        
    {
        private readonly IRatingRepositoryReadOnly _ratingRepositoryReadOnly;
        private readonly IUserRepositoryReadOnly _userRepositoryReadOnly;
        private readonly IMovieRepositoryReadOnly _movieRepositoryReadOnly;

        public RatingHandler(IRatingRepositoryReadOnly ratingRepositoryReadOnly, IUserRepositoryReadOnly userRepositoryReadOnly,
            IMovieRepositoryReadOnly movieRepositoryReadOnly)
        {
            _ratingRepositoryReadOnly = ratingRepositoryReadOnly;
            _userRepositoryReadOnly = userRepositoryReadOnly;
            _movieRepositoryReadOnly = movieRepositoryReadOnly;
        }

        public async Task<ICommandResult> Handle(RegisterRatingCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                    return new CommandResult(false, Message.Inconsistencias_dados, command.Notifications);

                if (!await _userRepositoryReadOnly.CheckIdAsync(command.AccountId))
                    AddNotification("UserId", Message.Usuario_nao_cadastrado);

                if (!await _movieRepositoryReadOnly.CheckIdAsync(command.MovieId))
                    AddNotification("MovieId", Message.filme_nao_cadastrado);

                if (await _userRepositoryReadOnly.CheckUsuarioVotouAsync(command.AccountId))
                    AddNotification("AccountId", Message.usuario_votou);

                if (Invalid)
                    return new CommandResult(false, Message.Inconsistencias_dados, Notifications);

                Rating rating = new Rating(command.AccountId, command.MovieId, command.Points);

                await _ratingRepositoryReadOnly.VoteAsync(rating);

                return new CommandResult(true, Message.Operacao_sucesso,
                    new { rating.MovieId, rating.AccountId, rating.Points });
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
