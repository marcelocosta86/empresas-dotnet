﻿using IOASYS.Desafio.Application.Interfaces.CommandResult;
using IOASYS.Desafio.Application.ViewModel;
using IOASYS.Desafio.Application.ViewModel.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Application.Interfaces
{
    public interface IAccountAppService
    {
        Task<CommandResultViewModel> ObterToken(LoginViewModel login);
        Task<IEnumerable<UserViewModel>> ListAsync();
        Task<UserViewModel> GetByIdAsync(int id);
        Task<CommandResultViewModel> Insert(UserViewModel user);
        Task<CommandResultViewModel> Update(UserViewModel user);
        Task<CommandResultViewModel> Delete(int id);
    }
}
