﻿using Dapper;
using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Interfaces.Repository.ReadOnly;
using IOASYS.Desafio.Infra.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Infra.Repositories.ReadOnly
{
    public class RatingRepositoryReadOnly : IRatingRepositoryReadOnly
    {
        private readonly DynamicParameters _parametros = new DynamicParameters();
        private readonly IDbConnection conexao;

        public RatingRepositoryReadOnly(IOptions<SettingsInfra> options)
        {
            conexao = new SqlConnection(options.Value.ConnectionString);
        }

        public async Task VoteAsync(Rating rating)
        {
            try
            {
                _parametros.Add("AccountId", rating.AccountId, DbType.Int32);
                _parametros.Add("MovieId", rating.MovieId, DbType.Int32);
                _parametros.Add("Points", rating.Points, DbType.Int32);

                var sql = @"INSERT INTO Rating (AccountId, MovieId, Points) VALUES (@AccountId, @MovieId, @Points); SELECT SCOPE_IDENTITY();";

                await conexao.ExecuteScalarAsync<long>(sql, _parametros);
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
