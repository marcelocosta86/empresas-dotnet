﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.ViewModel
{
    public class UserViewModel
    {
        public int AccountId { get; set; }
        public string UserName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool Active { get; set; }
    }
}
