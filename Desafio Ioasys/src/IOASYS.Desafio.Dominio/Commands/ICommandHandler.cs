﻿using IOASYS.Desafio.Dominio.Interfaces.CommandResult;
using IOASYS.Desafio.Dominio.Validator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Commands
{
    public interface ICommandHandler<T>
        where T : ISelfValidator
    {
        Task<ICommandResult> HandlerAsync(T command);
    }
}
