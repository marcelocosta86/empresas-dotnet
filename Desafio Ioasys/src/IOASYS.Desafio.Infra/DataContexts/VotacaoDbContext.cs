﻿using IOASYS.Desafio.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;

namespace IOASYS.Desafio.Infra.DataContexts
{
    public class VotacaoDbContext : DbContext
    {
        public VotacaoDbContext(DbContextOptions options) : base(options)
        {
        }

        DbSet<Movie> Movie { get; set; }
        DbSet<Rating> Rating { get; set; }
        DbSet<Account> Account { get; set; }
        DbSet<Actor> Actor { get; set; }
    }
}
