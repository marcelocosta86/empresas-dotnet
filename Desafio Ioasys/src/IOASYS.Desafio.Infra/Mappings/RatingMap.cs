﻿using IOASYS.Desafio.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IOASYS.Desafio.Infra.Mappings
{
    public class RatingMap : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.HasKey(a => a.RatingId);

            builder.Property(a => a.RatingId).ValueGeneratedOnAdd().UseIdentityColumn();

            builder.Property(c => c.AccountId)
                .IsRequired();

            builder.Property(c => c.MovieId)
                .IsRequired();

            builder.Property(c => c.Points)
                .IsRequired();
        }    
    }
}
