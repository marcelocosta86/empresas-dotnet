﻿using MediatR;

namespace IOASYS.Desafio.Application
{
    public interface IQuery<out TResult> : IRequest<TResult>
    {

    }
}