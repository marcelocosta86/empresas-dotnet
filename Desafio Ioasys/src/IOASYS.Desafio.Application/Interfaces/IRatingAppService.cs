﻿using IOASYS.Desafio.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Application.Interfaces
{
    public interface IRatingAppService
    {
        Task<CommandResultViewModel> VoteAsync(RatingViewModel command);
    }
}
