﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOASYS.Desafio.Application.ViewModel
{
    public class CommandResultViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
