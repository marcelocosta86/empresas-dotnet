﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IOASYS.Desafio.Dominio.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Message {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Message() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IOASYS.Desafio.Dominio.Resources.Message", typeof(Message).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Antenticação realizada com sucesso.
        /// </summary>
        internal static string Autenticacao_sucesso {
            get {
                return ResourceManager.GetString("Autenticacao_sucesso", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preencha o campo obrigatorio.
        /// </summary>
        internal static string Campo_obrigatorio {
            get {
                return ResourceManager.GetString("Campo_obrigatorio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filme não encontrado.
        /// </summary>
        internal static string filme_nao_cadastrado {
            get {
                return ResourceManager.GetString("filme_nao_cadastrado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Corrija as inconsistencias na requisição.
        /// </summary>
        internal static string Inconsistencias_dados {
            get {
                return ResourceManager.GetString("Inconsistencias_dados", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Já existe um usuario registrado na base.
        /// </summary>
        internal static string Login_existente {
            get {
                return ResourceManager.GetString("Login_existente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login ou senha inválidos.
        /// </summary>
        internal static string Login_ou_Senha_invalidos {
            get {
                return ResourceManager.GetString("Login_ou_Senha_invalidos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operação realizada com sucesso.
        /// </summary>
        internal static string Operacao_sucesso {
            get {
                return ResourceManager.GetString("Operacao_sucesso", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pontuação acima do permitido.
        /// </summary>
        internal static string Pontuacao_Invalida {
            get {
                return ResourceManager.GetString("Pontuacao_Invalida", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campo excedeu o tamanho maximo.
        /// </summary>
        internal static string Tamanho_excedido {
            get {
                return ResourceManager.GetString("Tamanho_excedido", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuario não localizado.
        /// </summary>
        internal static string Usuario_nao_cadastrado {
            get {
                return ResourceManager.GetString("Usuario_nao_cadastrado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuário ja votou.
        /// </summary>
        internal static string usuario_votou {
            get {
                return ResourceManager.GetString("usuario_votou", resourceCulture);
            }
        }
    }
}
