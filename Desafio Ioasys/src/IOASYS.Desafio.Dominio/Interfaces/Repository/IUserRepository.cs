﻿using IOASYS.Desafio.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Dominio.Interfaces.Repository
{
    public interface IUserRepository
    {
        Task Add(Account user);
        void Update(Account user);
        void Remove(Account user);
    }
}
