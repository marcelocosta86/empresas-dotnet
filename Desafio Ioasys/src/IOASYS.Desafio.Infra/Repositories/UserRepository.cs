﻿using IOASYS.Desafio.Dominio.Entidades;
using IOASYS.Desafio.Dominio.Interfaces.Repository;
using IOASYS.Desafio.Infra.DataContexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace IOASYS.Desafio.Infra.Repositories
{
    public class UserRepository : IUserRepository
    {
        protected readonly VotacaoDbContext Db;
        protected readonly DbSet<Account> DbSet;

        public UserRepository(VotacaoDbContext context)
        {
            Db = context;
            DbSet = Db.Set<Account>();
        }

        public async Task Add(Account user)
        {
            await DbSet.AddAsync(user);
            await Db.SaveChangesAsync();
        }

        public void Remove(Account user)
        {
            user.SetInactive();
            DbSet.Update(user);
            Db.SaveChanges();
        }

        public void Update(Account user)
        {
            DbSet.Update(user);
            Db.SaveChanges();
        }
    }
}
